const Connection = require(__serverRoot + "irc/Connection");
const CouchEvents = require(__serverRoot + "CouchEvents");
const Channel = require(__serverRoot + "models/Channel");
const Query = require(__serverRoot + "models/Query");
const Message = require(__serverRoot + "models/Message");
const Logger = require(__serverRoot + "Logger");

function Network (connectionSettings) {
    this.name = connectionSettings.name;
    this.connection = new Connection(connectionSettings);
    this.channelBuffers = {};
    this.lobby = [];
    this.me = {};
    this.prefixes = {
        "q": "~",
        "a": "&",
        "o": "@",
        "h": "%",
        "v": "+",
    };

    this.log = Logger("Network " + this.name);

    let con = this.connection; // save typing it constantly

    con.on("server_options", () => {
        let options = this.connection.irc.network.options;
        if (options.PREFIX) {
            this.prefixes = {};

            for (let prefix of options.PREFIX) {
                this.prefixes[prefix.mode] = prefix.symbol;
            }
        }

        // TODO: there are more goodies in there, use them maybe?
    });

    con.on("registered", () => {
        // Autojoining channels
        this.log.info("Autojoining", Object.keys(connectionSettings.channels).join(", "));
        for (let channel in connectionSettings.channels) {
            let key = connectionSettings.channels[channel];
            this.join(channel, key);
        }

        CouchEvents.emit("irc.network.connected", this);
    });

    // Auto rejoin with numbers appended if nick is already in use
    // TODO: need a way to 'reclaim' a nick.
    // also '_'s are more common than appending numbers, but is useful for the default nick
    con.on("nick_in_use", (event) => {
        let newNick = connectionSettings.nick + Math.floor(Math.random() * 1000);
        con.irc.changeNick(newNick);
    });

    // Send messages to channels
    [
        "channel_info",
        "topic",
        "topicsetby",
        "join",
        "part",
        "kick",
        //"quit", -- doesn't have a target/channel - handled below
        "invited",
        "notice",
        "action",
        "privmsg",
        "userlist"
    ].forEach((eventType) => {
        con.on(eventType, (event) => {
            let target = (event.target || event.channel).toLocaleLowerCase();

            if (!target || target === "*" || (event.from_server && target === this.me.nick)) {
                // "Other" messages
                let msg = new Message(event);
                if (msg.from_server) {
                    msg.type = "servermsg";
                }
                this.lobby.push(msg);
            }
            else if (target === con.irc.user.nick) {
                // Query
                if (! (target in this.channelBuffers)) {
                    this.channelBuffers[target] = new Query(event.nick);
                }
                this.channelBuffers[target].handle(eventType, event);
            }
            else {
                // Channel message
                this.channelBuffers[target].handle(eventType, event);
            }
        });
    });

    con.on("nick", (event) => {
        for (let channel of Object.values(this.channelBuffers)) {
            channel.handle("nickchange", event);
        }

        if (event.new_nick === this.me.nick) {
            CouchEvents.emit("irc.nickchange.success", event);
        }
    });

    con.on("quit", (event) => {
        for (let channel of Object.values(this.channelBuffers)) {
            channel.handle("quit", event);
        }
    });

    [
        "unknown_command",
        "motd"
    ].forEach((eventType) => {
        con.on(eventType, (event) => {
            if (event.from_server) {
                event.type = "servermsg";
            }
            else if (!event.type) {
                event.type = eventType;
            }
            let msg = new Message(event);
            this.lobby.push(msg);
        });
    });

    con.on("mode", (event) => {
        let msg = new Message(event);
        msg.type = "mode";
        if (msg.target === this.me.nick) {
            this.lobby.push(msg);
        } else if (msg.target in Object.keys(this.channelBuffers)) {
            this.channelBuffers[msg.target].messages.push(msg);
        }
    });

    con.on("irc_error", (event) => {
        this.log.error("An IRC error occured: " + event.reason);
        let msg = new Message(event);
        msg.type = "irc_error";
        this.lobby.push(msg);
    });

    con.on("error_connecting", (event) => {
        let msg = new Message(event);
        msg.type = "error_connecting";
        this.lobby.push(msg);
    });

    con.on("whois", (event) => {
        event.network = this.name;
        CouchEvents.emit("irc.whois_reply", event);
    });

    con.on("nick_in_use", (event) => {
        if (con.registered) {
            CouchEvents.emit("irc.nickchange.inuse", event);
        }
    });

    con.on("nick_invalid", (event) => {
        if (con.registered) {
            CouchEvents.emit("irc.nickchange.invalid", event);
        }
    });
}

Network.prototype.connect = function() {
    this.connection.connect(...arguments);
    this.me = this.connection.irc.user;
};

Network.prototype.disconnect = function() {
    this.connection.disconnect(...arguments);
};

Network.prototype.join = function (channel, key) {
    channel = channel.toLocaleLowerCase();
    this.channelBuffers[channel] = new Channel(channel, key, this.name);

    this.connection.irc.join(channel, key);
    this.connection.irc.raw("MODE", channel);
    CouchEvents.emit("irc.joined_channel", {network: this, channel: this.channelBuffers[channel]});
};

Network.prototype.part = function (channel, msg) {
    channel = channel.toLocaleLowerCase();
    //this.channelBuffers[channel].cleanup();
    delete this.channelBuffers[channel];

    this.connection.irc.part(channel, msg);
};

Network.prototype.changeNick = function (newNick) {
    this.connection.irc.changeNick(newNick);
};

Network.prototype.getInfo = function () {
    var info = Object.assign({}, {
        name: this.name,
        lobby: this.lobby,
        channels: {},
        user: this.me,
        prefixes: this.prefixes,
    });

    for (let channel of Object.values(this.channelBuffers)) {
        info.channels[channel.channel] = channel.getInfo();
    }

    return info;
};

module.exports = Network;