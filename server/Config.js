const fse = require("fs-extra");

const CouchEvents = require("./CouchEvents");
const log = require("./Logger")("Config");

CouchEvents.on("couch.shutdown", () => {
    saveSync();
});

let filePath;
let config = {};

function load(path) {
    log.info("Loading config from", path);
    filePath = path;
    var defaultConfig = fse.readJsonSync("./default_config.json");
    try {
        var userConfig = fse.readJsonSync(filePath);
    } catch (err) {
        if (err.errno === -2) {
            /* File not found */
            config = defaultConfig;
            save();
        }
        else {
            log.error("Reading config file error: ", err);
        }
    }

    if (parseInt(userConfig.configVersion) < parseInt(defaultConfig.configVersion) || (userConfig.configVersion || defaultConfig.configVersion) === undefined) {
        updateConfig(userConfig);
    }

    Object.assign(config, defaultConfig, userConfig);

    if (!userConfig.configVersion) {
        log.info("Config file updated to version " + config.configVersion);
        save();
    }
    return config;
}

function save(path, cb) {
    filePath = filePath || path;
    fse.writeJson(filePath, config, {spaces: 4});

    if (cb && typeof cb === "function") {
        cb();
    }
}

function saveSync(path) {
    filePath = filePath || path;
    fse.writeJsonSync(filePath, config, {spaces: 4});
}

function replaceConfig(newConfig) { // Needed Config.config (exported value) !== config (our value)
    config = newConfig;
}

function updateConfig(userConfig) {
    switch (parseInt(userConfig.configVersion || "0")) {
    // oldest versions at the top, with no `break` except the one before default
        case 0:
            // The config had no version, but we still want to catch it to update the config!
        case 1:
            // Changed network.channels from an array to an object with the form {channel: channelkey}
            // (we did not have support for loading channel keys before this)
            for (let network of Object.values(userConfig.networks || {})) {
                let oldChannels = network.channels.slice(); // copy it
                network.channels = {};
                for (let channel of oldChannels) {
                    network.channels[channel] = "";
                }
            }
            break;
        default:
            log.error("Could not update config!");
            return;
    }

    delete userConfig.configVersion;
}


module.exports = {
    load,
    save,
    saveSync,
    config,
    replaceConfig,
};
