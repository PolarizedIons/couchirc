const assert = require("assert");
const mock = require("mock-require");
const fse = require("fs-extra");
var Config;

const LogSilencer = (prefix) => {
    return {
        info: () => {return;},
        error: (err) => { console.error("[Config Error] ", err); },
        debug: () => {return;},
        setShowDebug: () => {return;}
    };
};

mock("../Logger", LogSilencer);

process.chdir(__dirname);

describe("Config", function() {
    beforeEach(function() {
        delete require.cache[require.resolve("../Config")]; // remove cached object so we can reimport it -- http://stackoverflow.com/a/16060619
        Config = require("../Config");
    });

    afterEach(function() {
        try {
            fse.removeSync("./removeme.json");
        } catch (err) { /* It doesn't exist */ }
    });

    describe("Config.js", function() {
        it("should only expose what it needs to", function() {
            let configKeys = Object.keys(Config);
            let expectedKeys = ["load", "save", "saveSync", "config", "replaceConfig"];

            assert.deepEqual(configKeys, expectedKeys);
        });
    });

    describe("#load()", function() {
        it("should load the default config automaticly", function() {
            fse.writeFileSync("./testconfigempty.json", "{}");
            Config.load("./testconfigempty.json");

            let expectedConfig = fse.readJsonSync("./default_config.json");
            assert.deepEqual(Config.config, expectedConfig);
        });

        it("should load the spesfied config file", function() {
            Config.load("./testconfig.json");
            let expected = {
                "obj": {
                    "obj1": 1,
                    "obj2": 2
                },
                "arr": [
                    1,
                    2,
                    3
                ],
                "one": 3,
                "two": 2,
                "three": 1,
                "hi": "hi",
                "nessled": {
                    "thing": {
                        "one": 1,
                        "two": 2
                    }
                },
                "configVersion": 999999999
            };

            assert.deepEqual(Config.config, expected);
        });
    });

    describe("#save()", function() {
        it("should call the callback when done", function(done) {
            this.timeout(3000);
            Config.save("./removeme.json", done);
        });

        it("should save the config", function() {
            this.timeout(3000);
            let expected = {
                "one": 1,
                "two": 2,
                "three": 3,
                "test": "succeeded"
            };

            Config.replaceConfig(expected);

            Config.save("./removeme.json", function() {
                setTimeout(() => { // Wait a tiny bit before reading the file we just saved
                    let savedConfig = fse.readJsonSync("./removeme.json", "utf-8");
                    assert.deepEqual(savedConfig, expected);
                }, 200);
            });
        });
    });

    describe("#saveSync", function() {
        it("should save the config", function() {
            let expected = {
                "one": 1,
                "two": 2,
                "three": 3,
                "test": "succeeded"
            };

            Config.replaceConfig(expected);

            Config.saveSync("./removeme.json");
            let savedConfig = fse.readJsonSync("./removeme.json");

            assert.deepEqual(savedConfig, expected);
        });
    });

    describe("updating config", function() {
        it("should add a configVersion to files without it", function() {
            //fse.writeFileSync("./testconfigempty.json", "{}");
            Config.load("./testconfigempty.json");
            assert.ok(Config.config.configVersion > 0);
        });

        // Add tests for upgrades here
        describe("1 -> 2; added channel keys", function() {
            it("should change the list of channels to objects with the channels as keys", function () {
                fse.writeJSONSync("./removeme.json", {
                    "networks": {
                        "freenode": {
                            "channels":["##CouchIRC", "##test"]
                        }
                    },
                    "configVersion": 1
                });

                let expected = {
                    freenode: {
                        channels: {
                            "##CouchIRC": "",
                            "##test": ""
                        }
                    }
                };

                Config.load("./removeme.json");
                assert.deepEqual(Config.config["networks"], expected);
            });
        });
    });
});