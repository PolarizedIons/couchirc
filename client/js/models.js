/* exported Network Channel Query */

function Network(data) {
    this.name = data.name;
    this.channels = data.channels || {};
    this.lobby = [];
}

function Channel(data) {
    this.channel = data.channel;
    this.messages = data.messages || [];
    this.users = data.users || [];
    this.createdAt = data.createdAt || 0;
    this.modes = data.modes || [];
    this.topic = data.topic || "";
    this.topicSetBy = data.topicSetBy || {}; // TODO: proper defaults
    this.url = data.url || "";
}

function Query(data) {
    this.recipient = data.recipient;
    this.messages = data.messages || [];
}
