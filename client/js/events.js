/* exported CouchEvents */
var CouchEvents = {
    events: {},
    on: function (event, callback) {
        if (! (event in this.events)) {
            this.events[event] = [];
        }
        this.events[event].push(callback);
    },
    emit: function (event, data) {
        //console.debug("Emitting event: " + event);
        if (event in this.events) {
            var callbacks = this.events[event];
            for (let cb of callbacks) {
                cb(data);
            }
        }
    }
};
