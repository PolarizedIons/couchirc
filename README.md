# Couch
A browser based IRC client that runs on node.js. Inspired by [TheLounge](https://github.com/thelounge/lounge/).

Requires a recent node version and a modern browser

# NOTE:
The project is not currently in a state where even I would considder it a 'client'. But check back soon! I'm working on it almost every day

## Installing
clone the repo into a directory
run `npm install` and `bower install`
(bower can be installed by running `npm install -g bower`)

## Running
after that, you can run it with `npm run start` or `node index.js` and close it (crtl+c).
The config file sould have been generated in the same folder. Check `Config.md` for documentation/what everything is
